<?php

return [
    'default' => 'mysql',
    'migrations' => 'migrations',
    'connections' => [
        'mysql' => [
            'driver' => env('DB_CONNECTION'),
            'host' => env('DB_HOST'),
            'port' => env('DB_PORT'),
            'database' => env('DB_DATABASE'),
            'username' => env('DB_USERNAME'),
            'password' => env('DB_PASSWORD'),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            /* 'options' => [
                 PDO::ATTR_PERSISTENT => true,
             ],*/
            /*'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',*/
        ]
    ]
];

/*
 *


 * */
