<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    //return $router->app->version();
    return "";
});

/*
 *
 * UNAUTHENTICATED ROUTES
 *
 */
$router->post('/login', 'AuthController@login');
$router->post('/register', 'AuthController@register');
$router->get('/test', function () {
    return 'OK';
});
$router->get('/test2', 'ExampleController@test');


/*
 *
 * AUTHENTICATED ROUTES
 *
 */
$router->group(['middleware' => 'auth',], function ($router) {
    $router->post('/logout', 'AuthController@logout');
    $router->post('/profile', 'UsersController@profile');
    $router->get('/dashboard', function () use ($router) {
        return "Test";
    });
});

$router->group(['middleware' => ['auth', 'permission:users_list'],], function ($router) {
    $router->post('/users', 'UsersController@lister');
});
$router->group(['middleware' => ['auth', 'permission:users_add'],], function ($router) {
    $router->post('/users/add', 'UsersController@add');
});
$router->group(['middleware' => ['auth', 'permission:users_view'],], function ($router) {
    $router->post('/users/view', 'UsersController@view');
});
$router->group(['middleware' => ['auth', 'permission:users_update'],], function ($router) {
    $router->post('/users/update', 'UsersController@update');
});
$router->group(['middleware' => ['auth', 'permission:users_delete'],], function ($router) {
    $router->post('/users/delete', 'UsersController@delete');
});

