# Requirements

- PHP 8.1+ (https://www.php.net/downloads.php)
- Docker (https://docs.docker.com/get-docker/)
- Composer (https://getcomposer.org/)

#Commands

### Install
composer install

### Initialisation
composer run-script init-dev

####(Pulls and creates an offical mysql Docker db. server image for development, migrate db.)

### Start
composer run-script start-dev

####(Run an offical mysql Docker db. server for development)

### Stop

composer run-script stop-dev

####(Stop the offical mysql Docker db. server for development)

###Remove

composer run-script remove-dev

####(Remove the offical mysql Docker db. server and image for development)

### Run Unit tests

composer run-script tests-dev

### Release

####(Create the Docker api server container for production)

### Db migrate

php artisan migrate

#Docs, license

### Sen CMS


- ### License
  Proprietary licence:
  Copyright 2022 Asoka-Success Kft., all rights reserved.

### Lumen PHP Framework

- Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

- ### License
    The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
