<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::create(['name' => 'admin']);
        $role_moderator = Role::create(['name' => 'moderator']);
        $role_user = Role::create(['name' => 'user']);

        $users_edit_permission = Permission::create(['name' => 'users_update']);
        $users_delete_permission = Permission::create(['name' => 'users_delete']);
        $users_add_permission = Permission::create(['name' => 'users_add']);
        $users_view_permission = Permission::create(['name' => 'users_view']);
        $users_view_permission = Permission::create(['name' => 'users_list']);

        $role_admin->givePermissionTo('users_update');
        $role_admin->givePermissionTo('users_delete');
        $role_admin->givePermissionTo('users_add');
        $role_admin->givePermissionTo('users_view');
        $role_admin->givePermissionTo('users_list');

        User::factory()->count(1)->withDefaults()->create();
        $admin = User::find(1)->firstOrFail();
        $admin->assignRole('admin');

        User::factory()->count(100)->create();
        $users = User::where("id", ">", 1)->get();
        foreach ($users as &$u) {
            $u->assignRole('user');
        }
    }
}
