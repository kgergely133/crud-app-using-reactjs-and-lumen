<?php

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    public static array $headers=[];

    /**
     * @return void
     */
    public function testServerIsUp()
    {
        $this->get('/test');
        $this->assertEquals("OK" , $this->response->getContent());
        //if($this->show_response){print_r($this->response->getContent());}
    }

    /**
     * @depends testServerIsUp
     */
    public function testRegistrationRequest()
    {   //seeJsonEquals
        //skip attached event handlers like email sending etc.

        //delete if exists
        $user = (new User)->where('email', 'unittest@unittest.hu')->first();
        print_r($user);
        if($user!=null){$user->delete();}

        $this->withoutEvents();
        //$this->expectsEvents('App\Events\UserRegistered');
        $response = $this->json('POST', '/register', ['name' => 'Árvíztűrő tükörfúrógép','email' => 'unittest@unittest.hu','password' => 'TestTest']);
        $response->seeJson(['message' => "CREATED",]);
        $this->seeInDatabase(User::getTableName(), ['email' => 'unittest@unittest.hu']);
    }

    /**
     * @depends testRegistrationRequest
     */
    public function testLogin()
    {
        $response = $this->json('POST', '/login', ['email' => 'unittest@unittest.hu','password' => 'TestTest']);
        $response->seeJson(['token_type' => "bearer",]);
        $ret=json_decode($this->response->getContent(),true);
        if(isset($ret["token"])){UserTest::$headers=["Authorization"=>"Bearer ".$ret["token"]];}
    }

    /**
     * @depends testLogin
     */
    public function testProfile()
    {
        $response = $this->json('GET', '/profile', [],UserTest::$headers);
        $response->seeJson(['message' => "OK",]);
    }

    /**
     * @depends testProfile
     */
    public function testRefreshToken()
    {
        print_r(UserTest::$headers);
        $response = $this->json('POST', '/refresh', [],UserTest::$headers);
        echo $this->response->getContent();
        $this->assertTrue(true);
    }

    /**
     * @depends testRefreshToken
     */
    public function testLogout()
    {
        $this->assertTrue(true);
    }
}
