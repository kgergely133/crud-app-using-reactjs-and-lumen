FROM php:8.1.4-fpm

ENV PHP_OPCACHE_VALIDATE_TIMESTAMPS="1" \
    PHP_OPCACHE_MAX_ACCELERATED_FILES="1000000" \
    PHP_OPCACHE_MEMORY_CONSUMPTION="256" \
    PHP_OPCACHE_MAX_WASTED_PERCENTAGE="15"

RUN docker-php-ext-install opcache
RUN docker-php-ext-install pdo_mysql

RUN apt-get update
RUN apt-get install iputils-ping -y

COPY ./opcache.php.ini $PHP_INI_DIR/conf.d/opcache.ini

# Use the default production configuration
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

