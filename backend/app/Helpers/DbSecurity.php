<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class DbSecurity
{
    /**
     * Replace talbe or colum names
     */
    public static function tcr($data,$replace)
    {
        if (app()->environment('production') || true) {
            return $replace;
        }else{
            return $data;
        }
    }

    public static function se($data)
    {
        if (app()->environment('production') || true) {
            return DbSecurity::secured_encrypt($data);
        }else{
            return $data;
        }
    }

    public static function sd($data)
    {
        if (app()->environment('production') || true) {
            return DbSecurity::secured_decrypt($data);
        }else{
            return $data;
        }
    }

    private static function secured_encrypt($data)
    {
        $first_key = (env("APP_KEY"));
        $second_key = (env("JWT_SECRET"));

        $method = "aes-256-cbc";
        $iv_length = openssl_cipher_iv_length($method);
        $iv = openssl_random_pseudo_bytes($iv_length);

        $first_encrypted = openssl_encrypt($data,$method,$first_key, OPENSSL_RAW_DATA ,$iv);
        $second_encrypted = hash_hmac('sha3-512', $first_encrypted, $second_key, TRUE);

        $output = base64_encode($iv.$second_encrypted.$first_encrypted);
        return $output;
    }

    private static function secured_decrypt($input)
    {
        $first_key = (env("APP_KEY"));
        $second_key = (env("JWT_SECRET"));
        $mix = base64_decode($input);

        $method = "aes-256-cbc";
        $iv_length = openssl_cipher_iv_length($method);

        $iv = substr($mix,0,$iv_length);
        $second_encrypted = substr($mix,$iv_length,64);
        $first_encrypted = substr($mix,$iv_length+64);

        $data = openssl_decrypt($first_encrypted,$method,$first_key,OPENSSL_RAW_DATA,$iv);
        $second_encrypted_new = hash_hmac('sha3-512', $first_encrypted, $second_key, TRUE);

        if (hash_equals($second_encrypted,$second_encrypted_new))
            return $data;

        return false;
    }

    private static function  clean_str($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public static function esc($str){
        return DB::connection()->getPdo()->quote($str);
    }

    /**
     * Escape special characters for a LIKE query.
     *
     * @param string $value
     * @param string $char
     *
     * @return string
     */
    public static function escape_like(string $value, string $char = '\\'): string
    {
        return str_replace(
            [$char, '%', '_'],
            [$char.$char, $char.'%', $char.'_'],
            $value
        );
    }
}
