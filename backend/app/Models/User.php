<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Helpers\DbSecurity as ds;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable, HasFactory, HasRoles;

    protected $table = 'users';
    protected $table_sec = 'users';
    protected $cols = ['name' => "name", 'email' => "email", 'id' => "id", 'created_at' => "created_at"];

    public function __construct(array $attributes = array())
    {
        //$attributes["table"]=ds::tcr( $this->table,$this->table_sec);
        $this->table = ds::tcr($this->table, $this->table_sec);
        parent::__construct($attributes);
    }

    public function Col($col)
    {
        return $this->cols[$col];
    }

    /**
     * Get real col for sql
     *
     * @var array
     */
    public function SCol($col)
    {
        return "`" . $this->cols[$col] . "`";
    }

    /**
     * The attributes that are mass assignable. Order can't change.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Retrieve the identifier for the JWT key.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function getTableName()
    {
        return (new self())->getTable();
    }

    public static function self()
    {
        return (new self());
    }

    public static function getColNum()
    {
        return (new self())->getTable();
    }
}
