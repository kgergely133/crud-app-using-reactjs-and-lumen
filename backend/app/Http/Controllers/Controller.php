<?php

namespace App\Http\Controllers;

use App\Helpers\DbSecurity;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * Helper function to format the response with the token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($data)
    {
        global $startTimer, $otherTimer;
        $temp = round(microtime(true), 2);
        $data["time"] = $temp - $startTimer;
        if (!empty($otherTimer)) {
            $data["timeQ"] = $otherTimer - $startTimer;
        }
        $token = auth()->refresh();
        return response()->json($data, 200, ['token' => $token]);
    }

    /**
     * Helper function to return the necessary pagination data, set sorting
     *
     * @return array
     */
    protected function handleCrudData($data, $request, $class)
    {
        //filtering
        $allowedFilterOps = ["=", "<", ">", ">=", "<=", "<>"];
        if (isset($request->filters)) {
            foreach ($request->filters as $s) {
                if ($s["dataType"] == "string") {
                    $data->where($class->Col($s["key"]), 'LIKE', '%' . DbSecurity::escape_like($s["filterRVR"]) . '%');
                } else {
                    if ($s["dataType"] == "number") {
                        if (isset($s["filterRowOperator"]) && !in_array($s["filterRowOperator"], $allowedFilterOps)) {
                            $s["filterRowOperator"] = $allowedFilterOps[0];
                        }
                        $data->where($class->Col($s["key"]), $s["filterRowOperator"], $s["filterRVR"]);
                    } else {
                        if ($s["dataType"] == "date") {
                            if (isset($s["filterRowOperator"]) && !in_array($s["filterRowOperator"],
                                    $allowedFilterOps)) {
                                $s["filterRowOperator"] = $allowedFilterOps[0];
                            }
                            $data->where($class->Col($s["key"]), $s["filterRowOperator"], $s["filterRVR"]);
                        }
                    }
                }
            }
        }

        //Adv.filtering
        if (isset($request->advFilters) && isset($request->advFilters["groupName"]) && isset($request->advFilters["items"]) && count($request->advFilters["items"]) > 0) {
            $s = $request->advFilters;
            if ($s["groupName"] == "or") {
                $data->whereRaw("1=2");//needed fix for logic
            }
            foreach ($s["items"] as $s1) {
                if (isset($s1["groupName"])) {
                    foreach ($s1["items"] as $s2) {
                        if (isset($s2["groupName"])) {
                            foreach ($s2["items"] as $s3) {
                                if (isset($s3["groupName"])) {
                                    //only 3 sublevels
                                } else {
                                    $this->addAdvFilterWhereCond($data, $s3, $class, $s2["groupName"]);
                                }
                            }
                        } else {
                            $this->addAdvFilterWhereCond($data, $s2, $class, $s1["groupName"]);
                        }
                    }
                } else {
                    $this->addAdvFilterWhereCond($data, $s1, $class, $s["groupName"]);
                }
            }
        }

        //sorting
        foreach ($request->sort as $s) {
            $data->orderBy($class->Col($s["key"]), $s["sortDirection"] == "ascend" ? "ASC" : "DESC");
        }

        //paging
        $datap = $data->paginate($request->pageSize);

        return [
            "success" => "true",
            "data" => $datap->items(),
            "total" => $datap->total(),
            "last_page" => $datap->lastPage()
        ];
    }

    protected function addAdvFilterWhereCond(&$data, $s, $class, $type)
    {
        if (!isset($s["field"])) {
            return;
        }
        $allowedFilterOps = ["=", "<", ">", ">=", "<=", "<>", "contains", "not-contains"];
        if ($type == "and") {
            if ($s["operator"] == "contains") {
                $data->where($class->Col($s["field"]), 'LIKE', '%' . DbSecurity::escape_like($s["value"]) . '%');
            } else {
                if ($s["operator"] == "not-contains") {
                    $data->where($class->Col($s["field"]), 'NOT LIKE',
                        '%' . DbSecurity::escape_like($s["value"]) . '%');
                } else {
                    if (isset($s["operator"]) && !in_array($s["operator"], $allowedFilterOps)) {
                        $s["operator"] = $allowedFilterOps[0];
                    }
                    $data->where($class->Col($s["field"]), $s["operator"], $s["value"]);
                }
            }
        } else {
            if ($s["operator"] == "contains") {
                $data->orWhere($class->Col($s["field"]), 'LIKE', '%' . DbSecurity::escape_like($s["value"]) . '%');
            } else {
                if ($s["operator"] == "not-contains") {
                    $data->orWhere($class->Col($s["field"]), 'NOT LIKE',
                        '%' . DbSecurity::escape_like($s["value"]) . '%');
                } else {
                    if (isset($s["operator"]) && !in_array($s["operator"], $allowedFilterOps)) {
                        $s["operator"] = $allowedFilterOps[0];
                    }
                    $data->orWhere($class->Col($s["field"]), $s["operator"], $s["value"]);
                }
            }
        }
    }
}
