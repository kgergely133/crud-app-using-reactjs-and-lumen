<?php

namespace App\Http\Controllers;

use App\Helpers\DbSecurity as ds;
use App\Models\User;
use Illuminate\Http\Request;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function test()
    {
        phpinfo();
        $str = "test";
        $str2 = ds::se($str);
        $str3 = ds::sd($str2);
        echo $str3;
    }

    public function test2(Request $request)
    {
        global $otherTimer;
        $otherTimer = round(microtime(true), 2);
        $u = User::self();
        $data = User::select($u->Col('name'), $u->Col('email'), $u->Col('id'), $u->Col('created_at'))
            ->where('deleted', 0);

        $datap = $data->paginate($request->pageSize);

        return $this->respondWithToken([
            "data" => $datap->items(),
            "total" => $datap->total(),
            "last_page" => $datap->lastPage()
        ]);

        return $this->respondWithToken($data->get());
    }
}
