<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function __construct()
    {

    }

    public function View(Request $request)
    {
        $u = User::self();
        $data = User::select($u->Col('name'), $u->Col('email'), $u->Col('id'))
            ->where('id', $request->get("id"))->get();
        return $this->respondWithToken(["success" => true, "data" => $data[0]]);
    }

    public function Update(Request $request)
    {
        $data = User::find($request->get("id"));
        $data->name = $request->get("name");
        $data->password = Hash::make($request->get("password"));
        $data->email = $request->get("email");
        $saved = false;
        try {
            $saved = $data->save();
        } catch (\Exception $e) {

        }
        return $this->respondWithToken(["success" => $saved]);
    }

    public function Add(Request $request)
    {
        $data = new User();
        $data->name = $request->get("name");
        $data->password = Hash::make($request->get("password"));
        $data->email = $request->get("email");
        $data->deleted = false;
        $saved = false;
        try {
            $saved = $data->save();
        } catch (\Exception $e) {

        }
        return $this->respondWithToken(["success" => $saved]);
    }

    public function delete(Request $request)
    {
        $data = User::find($request->get("id"));
        $data->deleted = true;
        $saved = $data->save();
        return $this->respondWithToken(["success" => $saved]);
    }

    public function lister(Request $request)
    {//print_r($request->get("pageSize"));die();
        global $otherTimer;

        $otherTimer = round(microtime(true), 2);
        $u = User::self();
        $data = User::select($u->Col('name'), $u->Col('email'), $u->Col('id'), $u->Col('created_at'))
            ->where('deleted', 0);

        return $this->respondWithToken($this->handleCrudData($data, $request, User::self()));
    }

    public function profile(Request $request)
    {
        $return = ["roles" => Auth::user()->getRoleNames()];
        return $this->respondWithToken($return);
    }
}
