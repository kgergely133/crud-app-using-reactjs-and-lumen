//Main layout types
import React, {useContext, useState} from "react";
import {Route} from "react-router-dom";
import Preloader from "./components/Preloader";
import Sidebar from "./components/Sidebar";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import {globalContext} from "./routes";
import {ToastCont} from "./components/Toast";

export const RouteWithLoader = ({ component: Component, ...rest }) => {
    /*const [loaded, setLoaded] = useState(true);*/
    const { IsLoading } = useContext(globalContext);
    const { auth } = useContext(globalContext);

    /*useEffect(() => {
        const timer = setTimeout(() => setLoaded(true), 1);
        return () => clearTimeout(timer);
    }, []);*/

    let returnCode=auth.handleRooting(rest.routeobj,rest.path);

    return (
        returnCode!==""?returnCode:<Route {...rest} render={props => (
            <>
                {<Preloader show={IsLoading ? true : false} />}
                <ToastCont />
                <Component {...props} />
            </> ) } />
    );
};

export const RouteWithSidebar = ({ component: Component, ...rest }) => {
    /*const [loaded, setLoaded] = useState(true);*/
    const { IsLoading/*, setIsLoading */} = useContext(globalContext);
    const { auth } = useContext(globalContext);

    /*useEffect(() => {
        const timer = setTimeout(() => setLoaded(true), 1);
        return () => clearTimeout(timer);
    }, []);*/

    const localStorageIsSettingsVisible = () => {
        return localStorage.getItem('settingsVisible') === 'false' ? false : true
    }

    const [showSettings, setShowSettings] = useState(localStorageIsSettingsVisible);

    const toggleSettings = () => {
        setShowSettings(!showSettings);
        localStorage.setItem('settingsVisible', !showSettings);
    }

    let returnCode=auth.handleRooting(rest.routeobj,rest.path);

    return (
        returnCode!==""?returnCode:<Route {...rest} render={props => (
            <>
                {<Preloader show={IsLoading ? true : false} />}
                <Sidebar />
                <ToastCont />

                <main className="content">
                    <Navbar />
                    <Component {...props} />
                    <Footer toggleSettings={toggleSettings} showSettings={showSettings} />
                </main>
            </>
        )}
        />
    );
};