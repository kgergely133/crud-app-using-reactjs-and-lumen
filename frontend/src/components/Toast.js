import React, {useContext, useState} from "react";
import {Toast, ToastContainer} from "react-bootstrap";
import {globalContext} from "../routes";
import moment from "moment-timezone";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBell, faExclamationTriangle} from "@fortawesome/free-solid-svg-icons";

export const ToastCont= (props) => {
    return (
            <ToastContainer position="top-end" className="p-3" style={{zIndex: '100'}} >
                <globalContext.Consumer>
                    {({toasts, addToast}) => (
                        toasts.map((listItem)=>{ return(listItem)  })
                    )}
                </globalContext.Consumer>
            </ToastContainer>
       );
};

export const ToastElement = (props) => {
    const [showA, setShowA] = useState(true);
    const { toasts, setToasts } = useContext(globalContext);

    const toggleShowA = () =>{
        setShowA(!showA);
        setTimeout(()=>{//wait for animation
            let newtoasts:ToastElement = toasts.filter(item => (item.props.id !== props.id))
            setToasts(newtoasts);
            console.log(toasts.length);
        },500);
    }
    return(
        <Toast show={showA} onClose={toggleShowA} className={(props.type==="error"?" text-danger":"")} delay={(props.autohide>0?props.autohide:null)} autohide={(props.autohide>0?true:null)}>
            <Toast.Header>
                <span className="icon icon-xs rounded me-2">
                  <FontAwesomeIcon icon={props.type==="error"?faExclamationTriangle:faBell} className={"bell-shake"+(props.type==="error"?" text-danger":"")} />
                    <span className="icon-badge rounded-circle unread-notifications" />
                </span>
                <strong className={"me-auto"+(props.type==="error"?" text-danger":"")}>{props.title}</strong>
                <small className="text-muted">{moment().tz("Europe/Budapest").format('HH:mm')}</small>
            </Toast.Header>
            <Toast.Body>{props.body}</Toast.Body>
        </Toast>
    );
}