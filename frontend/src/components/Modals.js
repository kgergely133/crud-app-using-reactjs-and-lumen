import {Button, Modal} from "react-bootstrap";
import FilterControl from "../vendor/react-filter-control-2.1.0/src/components/FilterControl";
import React, {useState} from "react";


export function Confirmation(title:string,text:string,shown:boolean,...props){
    const [show, setShow] = useState(shown);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header closeButton>
                <Modal.Title>{title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className='top-element'>
                    {text}
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Cancel
                </Button>
                <Button variant="primary">Ok</Button>
            </Modal.Footer>
        </Modal>;
}