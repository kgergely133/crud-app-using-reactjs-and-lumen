import React, {useEffect, useState} from 'react';
import {Redirect, Switch} from "react-router-dom";

// pages
/*import Presentation from "./Presentation";*/
import DashboardOverview from "./pages/dashboard/DashboardOverview";
import Users from "./pages/Users";
import Settings from "./pages/Settings";
import BootstrapTables from "./pages/tables/BootstrapTables";
import Signin from "./pages/Signin";
import Signup from "./pages/Signup";
import ForgotPassword from "./pages/ForgotPassword";
import ResetPassword from "./pages/ResetPassword";
import Lock from "./pages/examples/Lock";
import ServerError from "./pages/examples/ServerError";

import {RouteWithLoader, RouteWithSidebar} from "./layouts";
import {AuthService, Groups} from "./authService";
import {Ajax} from "./helpers";
import moment from "moment";

export const Routes = {
    // pages
    //Presentation: { path: "/" },
    DashboardOverview: { path: "/",layout:RouteWithSidebar,component:DashboardOverview,needsLogin:true,allowedGroups:[Groups.User,Groups.Mod,Groups.Admin,Groups.SAdmin]},
    Users: {path: "/users",layout:RouteWithSidebar,component:Users,needsLogin:true,allowedGroups:[Groups.Admin,Groups.SAdmin] },
        UsersEdit: {path: "/users/edit/:id",layout:RouteWithSidebar,component:Users,needsLogin:true,allowedGroups:[Groups.Admin,Groups.SAdmin] },
        UsersNew: {path: "/users/new",layout:RouteWithSidebar,component:Users,needsLogin:true,allowedGroups:[Groups.Admin,Groups.SAdmin] },
        UsersView: {path: "/users/view/:id",layout:RouteWithSidebar,component:Users,needsLogin:true,allowedGroups:[Groups.Admin,Groups.SAdmin] },
    Settings: { path: "/settings" ,layout:RouteWithSidebar,component:Settings,needsLogin:true,allowedGroups:[Groups.User,Groups.Mod,Groups.Admin,Groups.SAdmin]},
    BootstrapTables: { path: "/tables/bootstrap-tables" ,layout:RouteWithSidebar,component:BootstrapTables,needsLogin:true,allowedGroups:[Groups.User,Groups.Mod,Groups.Admin,Groups.SAdmin]},
    Profile: { path: "/profile" ,layout:RouteWithSidebar,component:BootstrapTables,needsLogin:true,allowedGroups:[Groups.User,Groups.Mod,Groups.Admin,Groups.SAdmin]},
    Signin: { path: "/login",layout:RouteWithLoader,component:Signin,needsLogin:false },
    Signup: { path: "/examples/sign-up",layout:RouteWithLoader,component:Signup,needsLogin:false },
    ForgotPassword: { path: "/examples/forgot-password",layout:RouteWithLoader,component:ForgotPassword ,needsLogin:false },
    ResetPassword: { path: "/examples/reset-password" ,layout:RouteWithLoader,component:ResetPassword,needsLogin:false },
    Lock: { path: "/examples/lock",layout:RouteWithLoader ,component:Lock,needsLogin:true,allowedGroups:[Groups.User,Groups.Mod,Groups.Admin,Groups.SAdmin]},
    ServerError: { path: "/examples/500",layout:RouteWithLoader,component:ServerError,needsLogin:true,allowedGroups:[Groups.User,Groups.Mod,Groups.Admin,Groups.SAdmin] },

    NotFound: { path: "/examples/404",layout:Redirect,needsLogin:false },
};

export const globalContext = React.createContext({});

//export default class MainLayout extends React.Component {
export const MainLayout = (props) => {
    const [toasts, setToasts] = useState([]);
    const [IsLoading, setIsLoading] = useState(false);
    const addToast = (toast)=>{  setToasts(toasts.concat([toast])); };
    const [ajax] = useState(new Ajax(addToast));
    const [auth] = useState((new AuthService(ajax)));
    ajax.setAuth(auth);
    const [rows, setRows] = useState([]);

    useEffect( () => {
        let af=async ()=>{
            await auth.checkRoles();
            ajax.setAuth(auth);

            let rows2 = [];
            for (const key in Routes) {
                if(Routes[key].layout===RouteWithLoader){
                    rows2.push(<RouteWithLoader key={key}  exact path={Routes[key].path} component={Routes[key].component} routeobj={Routes[key]} />);
                }else if(Routes[key].layout===RouteWithSidebar){
                    rows2.push(<RouteWithSidebar key={key}  exact path={Routes[key].path} component={Routes[key].component} routeobj={Routes[key]} />);
                }
            }
            setRows(rows2);
        };
        af();
    }, []);

    return (rows.length===0?<></>:
        <globalContext.Provider value={{toasts, setToasts, addToast, IsLoading, setIsLoading, ajax, auth} }>
            <Switch>
                {rows}
                <Redirect to={Routes.DashboardOverview.path}/>
            </Switch>
        </globalContext.Provider>
    )
}
