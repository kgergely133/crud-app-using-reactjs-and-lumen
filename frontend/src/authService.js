import React from "react";
import {Redirect} from "react-router-dom";
import {Routes} from "./routes";
import { Log} from "./helpers";
import Cookies from "universal-cookie/es6";

export const Groups={
    User:"user",Mod:"moderator",Admin:"admin",SAdmin:"sadmin"
};

export class AuthService extends React.Component{
    isLoggedInVar=false;
    userGroups=[];
    ajax;

    constructor(ajaxp) {
        super();
        this.ajax=ajaxp;
    }

    async checkRoles(){
        let cookies=new Cookies();
        let jwt_data=this.getCurrentUserJWT();
        if(jwt_data!==null){
            this.isLoggedInVar=true;
            let retval=await this.ajax.Post("/profile", {},true);
            if (retval!=null && typeof retval.headers["token"]!=="undefined" && retval.headers["token"].length>0) {
                this.userGroups=retval.data.roles;
            }
        }
    }

    isLoggedIn(){
        return this.isLoggedInVar;
    }

    getCurrentUserJWT() {
        let cookies=new Cookies();
        let cookie=cookies.get("token"); Log.d("User token: "+cookie);
        if(typeof cookie ==="undefined"){
            return null;
        }
        return cookie;
    }

    async getHashedCurrentUserJWT() {
        let jwt=this.getCurrentUserJWT();
        if(jwt==null){return null;}
        let hashed=await this.hash(await this.hash(jwt)+jwt.substring(0,5)+"xyz");
        return hashed;
    }

    async login(email, password) {
        let retval=await this.ajax.Post("/login", {email,password},false);
        if (this.storeToken(retval)) {
            this.isLoggedInVar=true;
            this.userGroups=retval.data.roles;
            console.log(this.userGroups);
            return true;
        }
        return false;
      }

      storeToken(response){
          let cookies=new Cookies();
          if (response!=null && typeof response.headers["token"]!=="undefined" && response.headers["token"].length>0) {
              console.log("storeToken");
              console.log(response.headers);
              cookies.set("token", response.headers["token"],{path :'/'});
              return true;
          }else{
              return false;
          }
      }

    async logout(servercall=true) {
        let cookies=new Cookies();
        if(servercall){await this.ajax.Post("/logout", {},true);}
        cookies.remove("token");
        this.isLoggedInVar=false;
        window.location.href=Routes.Signin.path;
    }

    register(email, password) {
        return this.ajax.Post( "/signup", {
          email,
          password
        });
      }

    handleRooting(routerobj,currpath){
        //Redirect if not logged in
        if((routerobj.needsLogin && !this.isLoggedIn())){
            return <Redirect to={{ pathname: Routes.Signin.path, state: { from: window.location.pathname } }} />;
        }//User Group check, redirect if not have any cross
        else if((routerobj.needsLogin && this.isLoggedIn()) && typeof routerobj.allowedGroups !== "undefined" && !this.userGroups.some(r=> routerobj.allowedGroups.includes(r)) ){
            return <Redirect to={{ pathname: Routes.DashboardOverview.path, state: { from: window.location.pathname } }} />;
        }//Redirect from login page if logged in
        else if(this.isLoggedIn() && currpath===Routes.Signin.path){
            return <Redirect to={{ pathname: Routes.DashboardOverview.path, state: { from: window.location.pathname } }} />;
        }//If not requires redirect
        else{return "";}
    }

    handleNavitems(currpath){
        for (const key in Routes) {
            if(Routes[key].path===currpath && typeof Routes[key].allowedGroups !== "undefined" && this.userGroups.some(r=> Routes[key].allowedGroups.includes(r))){
                return true;
            }
        }
        return false;
    }

    async hash(message,algo="SHA-256") {
        // encode as UTF-8
        const msgBuffer = new TextEncoder().encode(message);

        // hash the message
        const hashBuffer = await crypto.subtle.digest(algo, msgBuffer);

        // convert ArrayBuffer to Array
        const hashArray = Array.from(new Uint8Array(hashBuffer));

        // convert bytes to hex string
        const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
        return hashHex;
    }

    randomString(length) {
        let text = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for(let i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

    iv;
    async enc(text, key){
        let enc = new TextEncoder();
        this.iv = window.crypto.getRandomValues(new Uint8Array(16));
        let iv = this.iv;
        return await window.crypto.subtle.encrypt( {  name: "AES-CBC",  iv },   key,  enc.encode(text)  );
    }
    async dec(text, key){
        let iv = this.iv;
        let decrypted = await window.crypto.subtle.decrypt( {   name: "AES-CBC", iv}, key,  text );
        let dec = new TextDecoder();
        return dec.decode(decrypted);
    }
}

