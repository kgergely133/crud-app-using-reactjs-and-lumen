import {Config} from "./config";
import axios from "axios";
import React from "react";
import {ToastElement} from "./components/Toast";
import {t} from "i18next";

let posts_arr = [];

export class Ajax extends React.Component{
    addToast=null;
    auth=null;

    constructor(addToastp) {
        super();
        this.addToast=addToastp;
    }

    setAuth(authp){
        this.auth=authp;
    }

    //buffering because auth token change every request
    async Post(path,postdata,withAuth=true){
        let startime=+new Date().getTime();
        let index=new Date().getTime()+randomstr(5); /**/Log.d("Start post: "+index+" Time:"+startime);
        posts_arr.push(index);
        while ( posts_arr[0] !== index) {  await sleep(300); } /**/Log.d("Start post real: "+index+" Time diff:"+(new Date().getTime()-startime));
        let retval=await this.PostDo(path,postdata,withAuth,startime);
        posts_arr.shift(); /**/Log.d("End post real: "+index); Log.d(posts_arr);
        return retval;
    }

    async PostDo(path,postdata,withAuth=true,startime=new Date().getTime()) {
        const ac = new AbortController();
        axios.defaults.timeout = 10000;
        const timeout = setTimeout(() => { ac.abort();}, 10000);//10sec
        try {
            let jwt=this.auth.getCurrentUserJWT();
            let jwthashed=await this.auth.getHashedCurrentUserJWT();//alert(jwthashed);
            let headers={ "Content-Type": "application/json",'Sec-Fetch-Mode': 'no-cors',};
            if(withAuth && jwt!=null){headers.Authorization= "Bearer " + jwt;headers.Nonce= jwthashed;}
            Log.d("Start post real2: Time diff:"+(new Date().getTime()-startime));
            const resp = await axios({ method: "post",  url: Config.serverUrl +path, timeout: 10000,  headers: headers,data: postdata, signal: ac.signal });
            this.auth.storeToken(resp);
            return resp;
        } catch(error) {
            if (error.response) {
                Log.e(error);

                if(error.response.status===401){//force logout
                    if(path==="/login"){return null;}
                    else{await this.auth.logout(false);}
                }else if(error.response.status===500){//
                    this.addToast(<ToastElement title={t("toast.title_error")} body={t("toast.body_error")}  type={"error"} autohide={12000} key={new Date().getTime()} id={new Date().getTime()} />);
                }
            }else{
                Log.e(error);
                this.addToast(<ToastElement title={t("toast.title_error")} body={t("toast.body_error")}  type={"error"} autohide={12000} key={new Date().getTime()} id={new Date().getTime()} />);
            }
            return null;
        }finally{
            try {clearTimeout(timeout);} catch(e) {}
        }
    };
}

export class Log {
    static d(obj){
        if(Config.app_env==="local"){
            console.debug(obj);
        }
    }
    static l(obj){
        console.log(obj);
    }
    static e(obj){
        console.error(obj);
    }
}

const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
}

const randomstr = (length)  => {
    var result           = [];
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result.push(characters.charAt(Math.floor(Math.random() *
            charactersLength)));
    }
    return result.join('');
}