import React, {useContext, useState} from "react";
import CrudTable from "./CrudTable";
import {Breadcrumb, Card} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faHome} from "@fortawesome/free-solid-svg-icons";
import Form from '../../vendor/react-bootstrap-form-generator/src'
import {globalContext, Routes} from "../../routes";
import {useHistory, useLocation} from "react-router-dom";


export function CrudNew (props){
    const location = useLocation();
    const { ajax } = useContext(globalContext);
    const [data, setData] = useState(null);
    const history = useHistory();

    const handleSubmit = async (data) => {
        console.log(data)
        let retval=await ajax.Post(props.CrudProps.basePath+"/add", data,true);
        if (retval!=null ) {
            console.log(retval);
            setData(null);
        }else{setData(-1);}
    }

    return <>
        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mt-4">
            <div className="d-block mb-4 mb-md-0">
                <Breadcrumb className="d-none d-md-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                    <Breadcrumb.Item onClick={()=>{history.push(Routes.DashboardOverview.path);}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                    <Breadcrumb.Item onClick={()=>{history.goBack()}}>{props.CrudProps.pageTitle}</Breadcrumb.Item>
                    <Breadcrumb.Item active>New</Breadcrumb.Item>
                </Breadcrumb>
            </div>
        </div>
        <h4 className={"mb-3"}>{props.CrudProps.pageTitle}: New</h4>
        <Card border="light" className="crud-form bg-white shadow-sm mb-4">
            <Card.Body>
                <div className="">
                    <div className="d-block mb-4 mb-md-0">
                        <p className="mb-0"></p>
                        <Form
                            verbose={false}
                            layout={props.NewLayout}
                            schema={props.NewSchema}
                            submitLabel="send it!"
                            onSubmit={(data) => handleSubmit(data)}
                        />
                    </div>
                </div>
            </Card.Body>
        </Card>
    </>
    ;
};