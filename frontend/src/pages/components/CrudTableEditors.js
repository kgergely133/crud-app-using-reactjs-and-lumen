import React from 'react';

import {updateFilterRowOperator, updateFilterRowValue} from 'ka-table/actionCreators';
import {kaDateUtils} from 'ka-table/utils';
import {FilterType} from "./CrudTable";
import Datetime from 'react-datetime';
import i18next from "i18next";
import i18n from "i18next";
import {DataType} from "ka-table/enums";

export const CustomLookupEditor = ({
                                       column, dispatch,
                                   }) => {
    const toNullableBoolean = (value) => {
        switch (value) {
            case 'true': return true;
            case 'false': return false;
            default: return value;
        }
    };
    return (
        <>
            <select
                className='form-control form-select'
                defaultValue={column.filterRowValue}
                style={{width: 100}}
                onChange={(event) => {
                    dispatch(updateFilterRowValue(column.key, toNullableBoolean(event.currentTarget.value)));
                }}>
                <option value=''/>
                <option value={'true'}>True</option>
                <option value={'false'}>False</option>
            </select>
        </>
    );
};

export const DefaultFilterOp="=";
export const FilterOperators = ({
                                    column, dispatch,
                                }) => {
    return (
        <select
            className='form-control form-select filter-ops'
            defaultValue={column.filterRowOperator}
            onChange={(event) => {
                dispatch(updateFilterRowOperator(column.key, event.currentTarget.value));
            }}>
            <option value={'='}>=</option>
            <option value={'<'}>{'<'}</option>
            <option value={'>'}>{'>'}</option>
            <option value={'<='}>{'<='}</option>
            <option value={'>='}>{'>='}</option>
            <option value={'<>'}>{'<>'}</option>
        </select>
    );
};

export const AdvFilterOperators = {
    "string":[{
        caption: 'Contains',
        name: 'contains',
    }, {
        caption: 'Does not Contain',
        name: 'doesNotContain',
    }],
    "number":[{
        caption: '=',
        name: '=',
    }, {
        caption: '<>',
        name: '<>',
    }, {
        caption: '>',
        name: '>',
    }, {
        caption: '<',
        name: '<',
    }],
    "date":[{
        caption: '=',
        name: '=',
    }, {
        caption: '<>',
        name: '<>',
    }, {
        caption: '>',
        name: '>',
    }, {
        caption: '<',
        name: '<',
    }],
    "object":[],
    "boolean":[{
        caption: '=',
        name: '=',
    }]
};

export const NumberEditor = ({
                                 column, dispatch,
                             }) => {
    const fieldValue =column.colTopFilter===FilterType.Remote? column.filterRVR:column.filterRowValue;
    return (
        <>
            <FilterOperators column={column} dispatch={dispatch}/>
            <input
                className={'form-control'}
                defaultValue={fieldValue}
                style={{width: 100}}
                onChange={(event) => {
                    const filterRowValue = event.currentTarget.value !== '' ? Number(event.currentTarget.value) : null;
                    dispatch(updateFilterRowValue(column.key, filterRowValue));
                }}
                type='number'
            />
        </>
    );
};

export const DateEditor = ({
                               column, dispatch,
                           }) => {
    const fieldValue =column.colTopFilter===FilterType.Remote? column.filterRVR:column.filterRowValue;
    const value = fieldValue && kaDateUtils.getDateInputValue(fieldValue);
    return (
        <>
            <FilterOperators column={column} dispatch={dispatch}/>
            <Datetime
                type='date'
                locale={i18n.language}
                timeFormat={false}
                className={''}
                value={value || ''}
                onChange={(event) => {
                    const targetValue = event;console.log("event");console.log(event);
                    const filterRowValue = typeof targetValue._d !=="undefined" ? new Date(targetValue._d) : null;
                    dispatch(updateFilterRowValue(column.key, filterRowValue));
                }}
            />
        </>
    );
};

export const DateTimeEditor = ({
                               column, dispatch,
                           }) => {
    const fieldValue =column.colTopFilter===FilterType.Remote? column.filterRVR:column.filterRowValue;
    const value = fieldValue && kaDateUtils.getDateInputValue(fieldValue);
    return (
        <>
            <FilterOperators column={column} dispatch={dispatch}/>
            <Datetime
                type='datetime'
                locale={i18n.language}
                timeFormat={true}
                className={'filter-dt'}
                value={fieldValue || ''}
                onChange={(event) => {
                    const targetValue = event;console.log("event");console.log(event);
                    const filterRowValue = typeof targetValue._d !=="undefined" ? new Date(targetValue._d) : null;
                    dispatch(updateFilterRowValue(column.key, filterRowValue));
                }}
            />
        </>
    );
};

export const TextEditor = ({
                               column, dispatch,
                           }) => {
    const fieldValue =column.colTopFilter===FilterType.Remote? column.filterRVR:column.filterRowValue;
    return (
        <>
            <input
                type='text'
                className={'form-control filter-text'}
                value={fieldValue || ''}
                onChange={(event) => {
                    const filterRowValue = event.currentTarget.value !== '' ? event.currentTarget.value : null;
                    dispatch(updateFilterRowValue(column.key, filterRowValue));
                }}
            />
        </>
    );
};
