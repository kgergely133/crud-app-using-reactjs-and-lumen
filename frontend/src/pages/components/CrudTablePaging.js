import * as React from 'react';



import {updatePageIndex} from "ka-table/actionCreators";
import PagingIndex from "ka-table/Components/PagingIndex/PagingIndex";
import {getPagesArrayBySize, getPagesForCenter} from "ka-table/Utils/PagingUtils";
import {getElementCustomization} from "ka-table/Utils/ComponentUtils";
import defaultOptions from "ka-table/defaultOptions";
import {IPagingProps} from "ka-table/props";
import Pagination from 'react-bootstrap/Pagination';

const PagingPagesCustom: React.FunctionComponent<IPagingProps> = (props) => {
    const {
        childComponents,
        dispatch,
        pagesCount,
        pageIndex = 0,
    } = props;
    const pages = getPagesArrayBySize(pagesCount);
    React.useEffect(() => {
        if (pageIndex !== 0 && pageIndex >= pages.length){
            dispatch(updatePageIndex(0));
        }
    }, [dispatch, pageIndex, pages]);

    const centerLengthNew=3;
    let isEndShown = true;//pageIndex < pages.length - centerLengthNew && pages.length > centerLengthNew + Math.ceil(centerLengthNew / 2);
    let isStartShown = true;//pageIndex >= centerLengthNew && pages.length > centerLengthNew + Math.ceil(centerLengthNew / 2);
    let centerPages = [];//getPagesForCenter(pages, isStartShown, isEndShown, pageIndex);
    centerPages.push(pages[pageIndex]);
    if( pageIndex!==pages.length-1 && pages.length>1){
        centerPages.push(pages[pageIndex+1]);
    }
    if(pageIndex!==0 && pages.length>1){
        centerPages.unshift(pages[pageIndex-1]);
    }

    const golast = ()=>{ dispatch(updatePageIndex(pages[pages.length - 1] )); }
    const gofirst = ()=>{ dispatch(updatePageIndex(pages[0] )); }
    const goprev = ()=>{ dispatch(updatePageIndex(pages[pageIndex-1<0?0:pageIndex-1] )); }
    const gonext = ()=>{ dispatch(updatePageIndex(pages[pageIndex+1>pages.length - 1?pages.length - 1:pageIndex+1] )); }

    return (
        <ul className={"pagination"}>

            {  (
                <>
                    <Pagination.First onClick={gofirst} />
                    <Pagination.Prev onClick={goprev} />
                        { isStartShown && pageIndex !== 0 &&
                        (
                            <>
                                {/*<PagingIndex {...props} pageIndex={0} isActive={pageIndex === 0} text={1}/>*/}
                                <PagingIndex {...props} pageIndex={centerPages[0] - 1<0?0:centerPages[0] - 1} isActive={false} text={'...'}/>
                            </>
                        )
                        }
                        {
                            centerPages.map((value, index) => {
                                return (
                                    <PagingIndex {...props} pageIndex={value} isActive={pageIndex === value} key={value} text={value + 1}/>
                                );
                            })
                        }
                        { isEndShown && pageIndex !== pages[pages.length - 1] &&
                        (
                            <>
                                <PagingIndex {...props} pageIndex={[...centerPages].pop() + 1>pages.length-1?pages.length-1:[...centerPages].pop() + 1} isActive={false} text={'...'}/>
                                {/*<PagingIndex {...props} pageIndex={pages[pages.length - 1]} isActive={pageIndex === pages[pages.length - 1]} text={pages[pages.length - 1] + 1}/>*/}
                            </>
                        )
                        }
                    <Pagination.Next onClick={gonext}  />
                    <Pagination.Last onClick={golast} />
                </>
            )}
        </ul>
    )
}

export default PagingPagesCustom;