import React from "react";
import CrudTable from "./CrudTable";
import {useLocation, useParams} from "react-router-dom";
import {CrudNew} from "./CrudNew";
import {CrudView} from "./CrudView";
import {CrudEdit} from "./CrudEdit";

export function Crud (props){
    const location = useLocation();
    let { id } = useParams();

    if(location.pathname.indexOf("/edit/")!==-1){
        return <CrudEdit EditSchema={props.NewSchema} EditLayout={props.NewLayout} CrudProps={props.CrudProps}  />;
    }else if(location.pathname.indexOf("/view/")!==-1){
        return <CrudView Id={id} CrudProps={props.CrudProps} />;
    }else if(location.pathname.indexOf("/new")!==-1){
        return <CrudNew NewSchema={props.NewSchema} NewLayout={props.NewLayout} CrudProps={props.CrudProps} />;
    }else{
        return <CrudTable tableProps={props.tableProps} cComps={props.cComps} CrudProps={props.CrudProps} />;
    }
};