import '../../scss/volt/components/_crudtable.scss';

import React, {useContext, useState} from 'react';

import {kaReducer, Table} from 'ka-table';
import {
    ActionType,
    DataType,
    EditingMode,
    FilteringMode,
    PagingPosition,
    SortDirection,
    SortingMode
} from 'ka-table/enums';
import {
    AdvFilterOperators,
    CustomLookupEditor,
    DateEditor,
    DateTimeEditor,
    DefaultFilterOp,
    NumberEditor,
    TextEditor
} from './CrudTableEditors';
import {filterData, IFilterControlFilterValue} from './CrudTableTopFilterEditors';
import FilterControl from '../../vendor/react-filter-control-2.1.0/src/components/FilterControl';//react-filter-control
import {Breadcrumb, Button, ButtonGroup, Col, Dropdown, Form, InputGroup, Modal, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faBan,
    faBell,
    faCheck, faCloudUploadAlt,
    faCog,
    faEnvelope,
    faFilter,
    faHome, faPlus, faRocket,
    faSearch, faTasks,
    faTrash, faUserShield
} from "@fortawesome/free-solid-svg-icons";
import {
    hideLoading,
    loadData,
    search,
    setSingleAction,
    showLoading,
    updateData, updateFilterRowValue, updatePageIndex,
    updatePagesCount, updatePageSize
} from 'ka-table/actionCreators';
import {globalContext, Routes} from "../../routes";
import {Log} from "../../helpers";
import {IPagingProps} from "ka-table/props";
import i18n, {t} from "i18next";
import CrudTablePaging from "./CrudTablePaging";
import {getUpdatedSortedColumns} from "ka-table/Utils/HeadRowUtils";
import { deleteRow } from 'ka-table/actionCreators';
import {ICellTextProps} from "ka-table/props";
import {Confirmation} from "../../components/Modals";
import {useHistory} from "react-router-dom";
import {faPencilAlt} from "@fortawesome/free-solid-svg-icons/faPencilAlt";

/*const dataArray = Array(119).fill(undefined).map(
    (_, index) => ({
        column1: index % 2 === 0,
        column2: `column:2 row:${index}`,
        column3: index % 5,
        column4: new Date(2022, 11, index),
        id: index,
    }),
);*/

export const FilterType ={
    Remote : "remote",
    Local : "local",
    None : null,
}

/**Top filter*/


const groups = [{
    caption: 'And',
    name: 'and',
}, {
    caption: 'Or',
    name: 'or',
}];
const filter: IFilterControlFilterValue = {
    groupName: 'and',
    items: [
       /* {
            field: 'name',
            key: '1',
            operator: 'contains',
            value: 'Tom',
        },
        {
            field: 'score',
            key: '2',
            operator: '>',
            value: '66',
        },*/
    ],
};

const PageSizeSelector: React.FC<IPagingProps> = ({ pageSize, pageSizes, dispatch }) =>  (
    <>
        <span>{t("crud_table.page_size")}</span>
        <select
            className='form-control form-select page-size-select'
            value={pageSize}
            onChange={(event) => {
                dispatch(updatePageSize(Number(event.currentTarget.value)));
            }}>
            {
                pageSizes?.map((value) => (<option key={value} value={value}>{value}</option>))
            }
        </select>
    </>
);


const customReducer = (prevProps, action, props) => {
    //fix sorting behaivor
    console.log(prevProps);console.log(action);
    let colindex=prevProps.columns.findIndex(p => p.key == action.columnKey);
    if(action.type===ActionType.UpdateSortDirection ){
        prevProps.columns[colindex].sortingClicked=((typeof prevProps.columns[colindex].sortingClicked==="undefined" || prevProps.columns[colindex].sortingClicked==null)
            ?1:prevProps.columns[colindex].sortingClicked+1);
        if(prevProps.columns[colindex].sortingClicked===3){
            prevProps.columns[colindex].sortingClicked=0;
            delete prevProps.columns[colindex].sortDirection;
            delete prevProps.columns[colindex].sortIndex;
            return { ...prevProps};
        }
        const sortedColumns = getUpdatedSortedColumns(prevProps.columns, action.columnKey, prevProps.sortingMode);
        return { ...prevProps, columns: sortedColumns };
    }else if(action.type===ActionType.UpdateFilterRowValue ){
        if(prevProps.columns[colindex].colTopFilter===FilterType.Remote){
            prevProps.columns[colindex].filterRVR=action.filterRowValue;
            return { ...prevProps};
        }
        return kaReducer(prevProps, action);
    }else if(action.type===ActionType.updateFilterRowOperator ){
        if(prevProps.columns[colindex].colTopFilter===FilterType.Remote){
            prevProps.columns[colindex].filterROP=action.filterRowOperator;
            return { ...prevProps};
        }
        return kaReducer(prevProps, action);
    } else{
        return kaReducer(prevProps, action);
    }
    //
};

const getSortingCols = (props) => {
    let cols =props.columns.filter( i =>i.sortIndex > 0 );
    cols.sort((a, b) => a.sortIndex - b.sortIndex)
    for (var i = 0; i < cols.length; i++) {  cols[i] = (({ sortDirection, key }) => ({ sortDirection, key }))(cols[i]); }
    return cols;
};

const getTopFilteringCols = (props) => {
    let cols =props.columns.filter( i =>(typeof i.filterRVR !=="undefined" && i.filterRVR !=="" && i.filterRVR !==null) );
    for (var i = 0; i < cols.length; i++) {
        if((cols[i].dataType===DataType.Number || cols[i].dataType===DataType.Date) && typeof cols[i].filterRowOperator==="undefined"){
            cols[i].filterRowOperator=DefaultFilterOp;
        }
        cols[i] = (({ filterRVR, key, dataType, filterRowOperator }) => ({ filterRVR, key, dataType, filterRowOperator }))(cols[i]);
    }
    return cols;
};




var glob_text_edit_timeout=-1;
const CrudTable = (props) => {
    let cCompsDefault = {
        table: {
            elementAttributes: () => ({
                className: 'table table-hover '//table-striped table-bordered
            })
        },
        tableHead: {
            elementAttributes: () => ({
                className: ''//thead-dark
            })
        },
        /*noDataRow: {
            content: () => 'No Data Found'
        },*/
        filterRowCell: {
            content: (props) => {
                const getEditor = () => {
                    if(typeof props.column.colTopFilter!="undefined" && props.column.colTopFilter!==FilterType.None){
                        if(props.column.dataType===DataType.String || props.column.editor===TextEditor){return <TextEditor {...props}/>;  }
                        else if(props.column.dataType===DataType.Date && (props.column.editor===DateEditor || typeof props.column.editor==="undefined" )){
                            return <DateEditor {...props}/>;}
                        else if(props.column.dataType===DataType.Date && props.column.editor===DateTimeEditor && props.column.filterROP!=="between"){ return <DateTimeEditor {...props}/>;}
                        else if(props.column.dataType===DataType.Number || props.column.editor===NumberEditor){ return <NumberEditor {...props}/>;}
                        else if(props.column.dataType===DataType.Boolean || props.column.editor===CustomLookupEditor){ return <CustomLookupEditor {...props}/>;}
                        else{return <></>;}
                    }else{return <></>;}
                }
                return <div className='d-flex'>{getEditor()}</div>;
            }
        },
        pagingIndex: {
            elementAttributes: ({ isActive }) => ({
                className: `page-item ${(isActive ? 'active' : '')}`
            }),
            content: ({ text }) => <div className='page-link'>{text}</div>
        },
        pagingPages: {
            elementAttributes: () => ({
                className: 'pagination '
            }),
        },
        paging: {
            elementAttributes: () => ({
                className: 'px-3 border-0 d-lg-flex align-items-center justify-content-between card-footer '
            }),
        },
        cellText: {
            content: (props2) => {
                switch (props2.column.key){
                    case ':delete': return <>
                            <a href={"javascript:void(0)"}>
                                <FontAwesomeIcon icon={faTrash}  className='delete-row-column-button' size={"lg"}
                                                 onClick={() => {setDeleteId(props2.rowKeyValue);setShowConfirm(true); }} />
                            </a>
                    </>;
                    case ':edit': return <>
                        <a href={"javascript:void(0)"}>
                            <FontAwesomeIcon icon={faPencilAlt}  className='edit-row-column-button' size={"lg"}
                                             onClick={() => {history.push(props.CrudProps.basePath+"/edit/"+props2.rowKeyValue); }} />
                        </a>
                    </>;
                }
            }
        },
        pagingSizes:{
            content: (props) => <PageSizeSelector {...props}/>
        }
    };

    const tablePropsDefault = {
        columns: [
        ],
        format: ({ column, value }) => {
            if (column.dataType===DataType.Date && (column.editor===DateEditor || typeof column.editor==="undefined" )){
                return value && value.toLocaleDateString(i18n.language, {month: '2-digit', day: '2-digit', year: 'numeric' });
            }else if (column.dataType===DataType.Date && column.editor===DateTimeEditor){
                return value && value.toLocaleDateString(i18n.language, {month: '2-digit', day: '2-digit', year: 'numeric', hour:'2-digit', minute:'2-digit', second:'2-digit' });
            }
        },
        paging: {
            enabled: true,
            pageSize: 10,
            pageIndex: 0,
            pageSizes: [10, 25, 50],
            position: PagingPosition.Bottom
        },
        /*data: [],*/
        editingMode: EditingMode.None,
        rowKeyField: 'id',
        sortingMode: SortingMode.MultipleRemote,
        filteringMode: FilteringMode.FilterRow,
        filter: ({ column }) => {//override filters
            /*if (column.key === 'prevScores') {
                return (value: number[], filterRowValue: number) => value.includes(Number(filterRowValue));
            }*/
        },
        singleAction: loadData(),
        loading: {  enabled: true },
        childComponents:cCompsDefault
    };

    //props.cComps.pagingPages= {  content: (props) => <CrudTablePaging {...props }/> };
    //const [Comps, setComps] = useState({...cCompsDefault,...props.cComps});
    const [tableProps, changeTableProps] = useState({...tablePropsDefault,...props.tableProps});
    const [filtersToggle, setfiltersToggle] = useState(getTopFilteringCols(tableProps).length>0);
    const { ajax } = useContext(globalContext);

    const filtersToggleSetter = () =>{setfiltersToggle(!filtersToggle);}

    const dispatch = async (action) => {
        changeTableProps((prevProps: ITableProps) =>{
                return customReducer(prevProps, action,tableProps) ;
            }
        );
        //Log.d("dispatch CrudTable "+tableProps.data.length);
        if (action.type === ActionType.LoadData) {
            await dispatch(showLoading(""));
            let result=await ajax.Post(props.CrudProps.basePath, {
                page: tableProps.paging.pageIndex+1,
                pageSize: tableProps.paging.pageSize,
                sort:getSortingCols(tableProps),
                filters:getTopFilteringCols(tableProps),
                advFilters:filterValue
            },true);

            await dispatch(updatePagesCount(result.data.last_page));
            await dispatch(updateData(result.data.data));
            await dispatch(hideLoading());
        }else if (action.type === ActionType.UpdateSortDirection || action.type === ActionType.UpdatePageIndex ||
            action.type === ActionType.UpdatePageSize ) {
            await dispatch(setSingleAction(loadData()));
        }else if (action.type === ActionType.UpdateFilterRowValue) {
            let colindex=tableProps.columns.findIndex(p => p.key === action.columnKey);
            if(tableProps.columns[colindex].colTopFilter===FilterType.Remote){
                if(tableProps.columns[colindex].dataType===DataType.String || tableProps.columns[colindex].dataType===DataType.Number
                    || (tableProps.columns[colindex].dataType===DataType.Date && tableProps.columns[colindex].editor===DateTimeEditor)){
                    try{clearTimeout(glob_text_edit_timeout)}catch (e){}
                    glob_text_edit_timeout = setTimeout(()=>{
                        dispatch(setSingleAction(loadData()));
                    }, 700,[]);
                }else{
                    dispatch(setSingleAction(loadData()));
                }
            }
        }else if(action.type === ActionType.UpdateFilterRowOperator){
            let colindex=tableProps.columns.findIndex(p => p.key === action.columnKey);
            if(tableProps.columns[colindex].colTopFilter===FilterType.Remote){dispatch(setSingleAction(loadData()));}
        }else if (action.type === ActionType.DeleteRow) {
            await dispatch(showLoading(""));
            setShowConfirm(false);
            let result=await ajax.Post(props.CrudProps.basePath+"/delete", {   id: deleteId, },true);
            await dispatch(setSingleAction(loadData()));
            await dispatch(hideLoading());
        }
    };

    /*useEffect( () => {
        changeTableProps(props.tableProps);
        return () => {   };
    }, [tableProps]);*/


    //og.d("Rebuild CrudTable "+tableProps.data.length);

    //routing
    const history = useHistory();

    /*modal*/
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    /*confirm modal*/
    const [showConfirm, setShowConfirm] = useState(false);
    const [deleteId, setDeleteId] = useState(null);
    const handleCloseConfirm = () => setShowConfirm(false);

    /*for advanced filter*/
    const [filterValue, changeFilter] = useState(filter);
    const onFilterChanged = (newFilterValue: IFilterControlFilterValue) => {
        changeFilter(newFilterValue);
        if(props.tableProps.filterType===FilterType.Remote){
            try{clearTimeout(glob_text_edit_timeout)}catch (e){}
            glob_text_edit_timeout = setTimeout(()=>{
                dispatch(setSingleAction(loadData()));
            },700);
        }
        //console.log(newFilterValue);
    };
    let fields = [];
    for(let i = 0; i < tableProps.columns.length; i++) {
        let col=tableProps.columns[i];
        if(col.colFilter!=="undefined" && col.colFilter===true){
            fields.push({caption: col.title,name: col.key,operators:AdvFilterOperators[col.dataType]});
        }
    }


    return (
        <>
            <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                <div className="d-block mb-4 mb-md-0">
                    <Breadcrumb className="d-none d-md-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                        <Breadcrumb.Item onClick={()=>{history.push(Routes.DashboardOverview.path);}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                        <Breadcrumb.Item active>{props.CrudProps.pageTitle}</Breadcrumb.Item>
                    </Breadcrumb>
                    <h4>{props.CrudProps.pageTitle}</h4>
                    <p className="mb-0">{props.CrudProps.pageDesc}</p>
                </div>

                <div className="btn-toolbar mb-2 mb-md-0">
                    <div className="mb-2">
                    <ButtonGroup>
                        <Button variant="outline-primary" size="sm">Share</Button>
                        <Button variant="outline-primary" size="sm">Export</Button>
                    </ButtonGroup>
                    </div>
                </div>
            </div>

            <div className="table-settings mb-4">
                <Row className="justify-content-between align-items-center">
                    <Col xs={12} sm={12} md={4} lg={3} xl={4}  className={"newButtonCont"}>
                        <div className="d-block mb-md-0 py-3 pt-0 mb-0 pb-0">
                            <Button variant="primary" size="sm" className="me-2" onClick={()=>{ history.push(props.CrudProps.basePath+"/new");}}>
                                <FontAwesomeIcon icon={faPlus} className="me-2" />New user
                            </Button>
                        </div>
                    </Col>

                    <Col xs={12} sm={12}  md={6} lg={4} xl={4} className={"searchTop"} style={{flex:"1"}}>
                        <div >
                            <InputGroup className={"searchTopButton"}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon={faSearch} />
                                </InputGroup.Text>
                                <Form.Control type="search" placeholder="Search" defaultValue={tableProps.searchText} onChange={(event) => {
                                    dispatch(search(event.currentTarget.value));
                                }} className='top-element' />
                            </InputGroup>
                        </div>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={2} xl={2} className="filtersToggle ps-md-0 text-end">
                        <Button variant="" size="sm" className={filtersToggle?'hidden':''} onClick={filtersToggleSetter}>
                            <span className="icon icon-sm icon-gray">
                              <FontAwesomeIcon icon={faFilter} transform="shrink-2"  />
                            </span>
                        </Button>
                        <Button variant="" size="sm" className={filtersToggle?'mt-n1':'hidden'} show={filtersToggle} onClick={filtersToggleSetter}>
                            <span className="fa-layers fa-fw">
                                <FontAwesomeIcon icon={faFilter}  />
                                <FontAwesomeIcon icon={faBan} transform="grow-14" />
                              </span>
                        </Button>


                        <Dropdown as={ButtonGroup} align={"end"}>
                            <Dropdown.Toggle split variant="link" className="text-dark m-0 p-0">
                                <span className="icon icon-sm icon-gray">
                                  <FontAwesomeIcon icon={faCog} />
                                </span>
                            </Dropdown.Toggle>
                            <Dropdown.Menu className="dropdown-menu-xs right0">
                                <Dropdown.Item className="fw-bold text-dark" onClick={() => setShow(true)}>Filters</Dropdown.Item>
                                <Dropdown.Item className="d-flex fw-bold">
                                    10 <span className="icon icon-small ms-auto"><FontAwesomeIcon icon={faCheck} /></span>
                                </Dropdown.Item>
                                <Dropdown.Item className="fw-bold">20</Dropdown.Item>
                                <Dropdown.Item className="fw-bold">30</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </Col>
                </Row>
            </div>

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Modal title</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className='top-element'>
                        <FilterControl {...{fields, groups, filterValue,  onFilterValueChanged: onFilterChanged}}/>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancel
                    </Button>
                    <Button variant="primary">Ok</Button>
                </Modal.Footer>
            </Modal>

            <Modal
                show={showConfirm}
                onHide={handleCloseConfirm}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>{"Delete row"}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className='top-element'> {"Are you sure?"}</div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseConfirm}> Cancel  </Button>
                    <Button variant="primary" onClick={()=>{dispatch(deleteRow())}}>Ok</Button>
                </Modal.Footer>
            </Modal>

            <div className='table-wrapper table-responsive shadow-sm card border-light' >
                <div className={'crud-table pt-0 card-body '+(filtersToggle?'show-filters':'')}>
                    <Table
                        {...tableProps}
                        extendedFilter={(data) => filterData(data, filterValue, props)}
                        dispatch={dispatch}
                    />
                </div>
            </div>
        </>
    );
};

export default CrudTable;