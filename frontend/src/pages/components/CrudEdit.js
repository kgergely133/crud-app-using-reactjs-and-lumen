import React, {useContext, useEffect, useState} from "react";
import CrudTable from "./CrudTable";
import {Breadcrumb, Card, Spinner} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faHome} from "@fortawesome/free-solid-svg-icons";
import Form from '../../vendor/react-bootstrap-form-generator/src/'
import {globalContext, Routes} from "../../routes";
import {useHistory, useLocation} from "react-router-dom";


export function CrudEdit (props){
    const location = useLocation();
    const { ajax } = useContext(globalContext);
    const [data, setData] = useState(null);
    const history = useHistory();

    let id = window.location.pathname.split("/")[window.location.pathname.split("/").length-1];

    useEffect( () => {
        async function fetchData() {
            let retval=await ajax.Post(props.CrudProps.basePath+"/view", {"id":id},true);
            if (retval!=null ) {
                console.log(retval);
                setData(retval.data.data);console.log(retval.data.data);
            }else{setData(-1);}
        }
        fetchData();
        return () => {
            // This is its cleanup.
        };
    }, []);

    const handleSubmit = async (data) => {
        console.log(data)
        data.id=id;
        let retval=await ajax.Post(props.CrudProps.basePath+"/update", data,true);
        if (retval!=null ) {
            console.log(retval);
        }else{setData(-1);}
    }

    return <>
        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mt-4">
            <div className="d-block mb-4 mb-md-0">
                <Breadcrumb className="d-none d-md-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                    <Breadcrumb.Item onClick={()=>{history.push(Routes.DashboardOverview.path);}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                    <Breadcrumb.Item onClick={()=>{history.goBack()}}>{props.CrudProps.pageTitle}</Breadcrumb.Item>
                    <Breadcrumb.Item active>Edit: {id}</Breadcrumb.Item>
                </Breadcrumb>
            </div>
        </div>
        <h4 className={"mb-3"}>{props.CrudProps.pageTitle} / Edit: {id}</h4>
        <Card border="light" className="crud-form bg-white shadow-sm mb-4">
            <Card.Body>
                <div className="">
                    <div className="d-block mb-4 mb-md-0">
                        <p className="mb-0"></p>
                        {data===null?<Spinner as="span" animation="border" size="sm" role="status" aria-hidden="true" className={"me-2 ms-n3"} />:
                            <Form
                                verbose={false}
                                layout={props.EditLayout}
                                schema={props.EditSchema}
                                submitLabel="send it!"
                                onSubmit={(data) => handleSubmit(data)}
                                defaultValue={data}
                            />
                        }
                    </div>
                </div>
            </Card.Body>
        </Card>
    </>
        ;
};