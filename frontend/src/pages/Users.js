import React, {useState} from "react";
import CrudTable, {FilterType} from "./components/CrudTable";
/*import {globalContext} from "../routes";*/
import {DataType, EditingMode, FilteringMode, PagingPosition, SortingMode} from "ka-table/enums";
import {Log} from "../helpers";
import {loadData} from "ka-table/actionCreators";
import {CustomLookupEditor, DateEditor, DateTimeEditor, NumberEditor, TextEditor} from "./components/CrudTableEditors";
import {Crud} from "./components/Crud";
import {Routes} from "../routes";



export default () => {
    const CrudProps = {
        pageTitle:"Users",
        pageDesc:"Users list",
        basePath:Routes.Users.path
    };

    const tableProps = {
        columns: [
            { key: 'id', title: 'Id', dataType: DataType.Number, colTopFilter:FilterType.Remote, colFilter:true, style: {minWidth: 130} },
            { key: 'created_at', title: 'Created', dataType: DataType.Date, colTopFilter:FilterType.Remote, colFilter:true, editor:DateTimeEditor, style: {width: 240} },
            { key: 'name', title: 'Name', dataType: DataType.String, colTopFilter:FilterType.Remote, colFilter:true, style: {minWidth: 130} },
            { key: 'email', title: 'E-mail', dataType: DataType.String, colTopFilter:FilterType.Remote, colFilter:true, style: {width: 240} },
            /* key: 'column3', title: 'Column 3', dataType: DataType.Number, filterRowOperator: '>', style: {width: 230}  },
            { key: 'column4', title: 'Column 4', dataType: DataType.Date, filterRowOperator: '<', style: {minWidth: 230} },*/
            { key: ':delete', width: 40, style: { textAlign: 'center',paddingRight:"10px",paddingLeft:"10px"   } },
            { key: ':edit', width: 40, style: { textAlign: 'center',paddingRight:"10px",paddingLeft:"10px"    }}
        ],
        rowKeyField: 'id',
        filterType:FilterType.Remote,

    };

    const NewSchema = {
        name: 'string',
        email: {
            type: 'string:email',
            label: 'Email',
            required:true
        },
        password: {
            type: 'string:password',
            label: 'Password',
            required:true
        },
        confirmPassword: {
            type: 'string:password',
            label: 'Confirm password',
            required:true
        },
    }
    const NewLayout = [
        [6],
        [6],
        [6],
        [6],
    ]
    const EditSchema = {
        name: 'string',
        email: {
            type: 'string:email',
            label: 'Email',
            required:true
        },
        password: {
            type: 'string:password',
            label: 'Password',
        },
        confirmPassword: {
            type: 'string:password',
            label: 'Confirm password',
        },
    }
    const EditLayout = [
        [6],
        [6],
        [6],
    ]

    const [TableProps, setTableProps] = useState(tableProps);

   /* useEffect( () => {
        getData();

        return () => {  };
    }, []);

    let getData= async ()=>{
        let retval=await ajax.Post(Routes.Users.path, {},true);
        if (retval!=null && retval.data ) {
            tablePropsDefault.data=retval.data;
            setTableProps({ ...tablePropsDefault });
        }
        else{setTableProps(tablePropsDefault);}
        Log.d(retval.data);
    }*/


    Log.d(TableProps);

  return (
      <>
          <Crud
              CrudProps={CrudProps}
              tableProps={tableProps}
              NewSchema={NewSchema}
              NewLayout={NewLayout}
          />
      </>

  );
};



/*

    country: {
        label: 'Select your country',
        type: 'select',
        options: [
            {
                label: 'Guatemala',
                value: 'GT'
            },
            {
                label: 'France',
                value: 'FR'
            }
        ]
    },
    time: {
        type: 'string:time',
        label: 'At what time?'
    },
    date: {
        type: 'string:date',
        label: 'Your birth date'
    },
    features: {
        type: 'checkbox',
        label: 'Select the features!',
        options: [
            { value: 'beta', label: 'Experimental!' },
            { value: 'stable', label: 'Normal'}
        ]
    },
    order: {
        type: 'radio',
        label: 'How many would you like to order?',
        options: [
            { value: 1, label: '1 - $10' },
            { value: 2, label: '2 - $40' },
            { value: 3, label: '3 - $60' }
        ]
    },
    comment: {
        type: 'textarea'
    }


    const NewLayout = [
    [4, 4, 4],
    [6, 6],
    [4, 4, 4],
    [12]
]
* */