import React, {useContext, useEffect, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faHome} from "@fortawesome/free-solid-svg-icons";
import {Breadcrumb} from 'react-bootstrap';

import {PageTrafficTable, RankingTable} from "../../components/Tables";
import {globalContext} from "../../routes";


export default () => {
    const { ajax } = useContext(globalContext);
    const [data, setData] = useState(null);

    useEffect( () => {
        async function fetchData() {
            let retval=await ajax.Post("/profile", {test:new Date().getTime()},true);
            if (retval!=null ) {
                console.log(retval);
                setData(retval);
            }else{setData(-1);}
        }
        fetchData();
        fetchData();
        fetchData();
        setTimeout(() => {  fetchData();}, 2000);
        setTimeout(() => {  fetchData();}, 4000);
        return () => {
            // This is its cleanup.
        };
    }, []);

  return (
    <>
      <div className="d-xl-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
        <div className="d-block mb-4 mb-xl-0">
            <div>
                {JSON.stringify(data)}
            </div>

          <Breadcrumb className="d-none d-md-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
            <Breadcrumb.Item><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
            <Breadcrumb.Item>Tables</Breadcrumb.Item>
            <Breadcrumb.Item active>Bootstrap tables</Breadcrumb.Item>
          </Breadcrumb>
          <h4>Bootstrap tables</h4>
          <p className="mb-0">
            Dozens of reusable components built to provide buttons, alerts, popovers, and more.
          </p>
        </div>
      </div>

      <PageTrafficTable />
      <RankingTable />
    </>
  );
};
