import React, {useContext, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEnvelope, faUnlockAlt} from "@fortawesome/free-solid-svg-icons";
import {faFacebookF, faGithub, faTwitter} from "@fortawesome/free-brands-svg-icons";
import {Button, Card, Col, Container, Form, FormCheck, InputGroup, Row, Spinner} from 'react-bootstrap';
import {Link, useHistory} from 'react-router-dom';

import BgImage from "../assets/img/illustrations/signin.svg";
import {globalContext, Routes} from "../routes";
import {t} from "i18next";

export default () => {
  const [validated, setValidated] = useState(false);
  const [serverError, setServerError] = useState("");
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  const { addToast } = useContext(globalContext);
  const { auth } = useContext(globalContext);

  const handleSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();

    const form = event.currentTarget;
    setValidated(true);
    setServerError("");
    if (form.checkValidity() === false) {

    }else{
      setLoading(true);
      auth.login(event.target.email.value,event.target.password.value,addToast).then((retval)=>{
          if(retval){ history.push(Routes.DashboardOverview); }
          else{
            setServerError(t("page_login.error_wrong_pass"));
            setValidated(false);
          }
      }).finally(()=>{ setLoading(false); })
    }
  };

  return (
    <main>
      <section className="d-flex align-items-center my-5 mt-lg-6 mb-lg-5">
        <Container>
          {/*<p className="text-center">
            <Card.Link as={Link} to={Routes.DashboardOverview.path} className="text-gray-700">
              <FontAwesomeIcon icon={faAngleLeft} className="me-2" /> Back to homepage
            </Card.Link>
          </p>*/}
          <Row className="justify-content-center form-bg-image" style={{ backgroundImage: `url(${BgImage})` }}>
            <Col xs={12} className="d-flex align-items-center justify-content-center">
              <div className="bg-white shadow-soft border rounded border-light p-4 p-lg-5 w-100 fmxw-500">
                <div className="text-center text-md-center mb-4 mt-md-0">
                  <h3 className="mb-0">Sign in to our platform</h3>
                </div>
                <Form className="mt-4" noValidate validated={validated} onSubmit={handleSubmit}>
                  <Form.Group id="email" className="mb-4">
                    <Form.Label>Your Email</Form.Label>
                    <InputGroup className={"has-validation"}>
                      <InputGroup.Text>
                        <FontAwesomeIcon icon={faEnvelope} />
                      </InputGroup.Text>
                      <Form.Control autoFocus required type="email" name={"email"} placeholder="example@company.com" />
                      <Form.Control.Feedback type="invalid">Please enter a valid email.</Form.Control.Feedback>
                    </InputGroup>
                  </Form.Group>
                  <Form.Group>
                    <Form.Group id="password" className="mb-4">
                      <Form.Label>Your Password</Form.Label>
                      <InputGroup className={"has-validation"}>
                        <InputGroup.Text>
                          <FontAwesomeIcon icon={faUnlockAlt} />
                        </InputGroup.Text>
                        <Form.Control required type="password" name={"password"} placeholder="Password" />
                        <Form.Control.Feedback type="invalid">Please enter a password.</Form.Control.Feedback>
                      </InputGroup>
                    </Form.Group>
                    <div className={"d-flex justify-content-between align-items-center mb-4 "+(serverError.length>0?"":"hidden")}>
                      <span className={"text-danger"}>{serverError}</span>
                    </div>
                    <div className="d-flex justify-content-between align-items-center mb-4">
                      <Form.Check type="checkbox" className={"no-validate"}>
                        <FormCheck.Input id="remember" name={"remember"} className="me-2" />
                        <FormCheck.Label htmlFor="remember" className="mb-0">Remember me</FormCheck.Label>
                      </Form.Check>
                      <Card.Link className="small text-end">Lost password?</Card.Link>
                    </div>
                  </Form.Group>
                  <Button variant="primary" type="submit" className="w-100" disabled={loading?true:false} >
                    {loading?<Spinner as="span" animation="border" size="sm" role="status" aria-hidden="true" className={"me-2 ms-n3"} />:""}
                    Sign in
                  </Button>
                </Form>

                <div className="mt-3 mb-4 text-center">
                  <span className="fw-normal">or login with</span>
                </div>
                <div className="d-flex justify-content-center my-4">
                  <Button variant="outline-light" className="btn-icon-only btn-pill text-facebook me-2">
                    <FontAwesomeIcon icon={faFacebookF} />
                  </Button>
                  <Button variant="outline-light" className="btn-icon-only btn-pill text-twitter me-2">
                    <FontAwesomeIcon icon={faTwitter} />
                  </Button>
                  <Button variant="outline-light" className="btn-icon-only btn-pil text-dark">
                    <FontAwesomeIcon icon={faGithub} />
                  </Button>
                </div>
                <div className="d-flex justify-content-center align-items-center mt-4">
                  <span className="fw-normal">
                    Not registered?
                    <Card.Link as={Link} to={Routes.Signup.path} className="fw-bold">
                      {` Create account `}
                    </Card.Link>
                  </span>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </main>
  );
};
